#!/bin/bash

# SCRIPT MADE BY #R@ymAn
# Creation date: 25/04/2023
# Version : 1.1


if [ $(ls $PROJECT_PATH | wc -l) -lt 2 ];
then
    shopt -s dotglob
    mv /opt/new_app/* $PROJECT_PATH
fi

rm -f $PROJECT_PATH/tmp/pids/server.pid

if [ "$NODE_ENABLED" = "true" ];
then
  yarn
fi

bundle install
rails db:migrate && rails db:seed && rails assets:precompile
bundle exec rails s -p ${PROJECT_PORT} -b '0.0.0.0'