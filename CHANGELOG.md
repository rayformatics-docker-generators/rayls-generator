
<a name="1.2.0"></a>
## [1.2.0](https://gitlab.com/rayformatics-docker-generators/rayls-generator/compare/1.1.3...1.2.0)

> 2023-06-15

### Feat

* Add dockerfile and docker-compose to generated project


<a name="1.1.3"></a>
## [1.1.3](https://gitlab.com/rayformatics-docker-generators/rayls-generator/compare/1.1.2...1.1.3)

> 2023-05-24


<a name="1.1.2"></a>
## [1.1.2](https://gitlab.com/rayformatics-docker-generators/rayls-generator/compare/1.1.1...1.1.2)

> 2023-05-24


<a name="1.1.1"></a>
## [1.1.1](https://gitlab.com/rayformatics-docker-generators/rayls-generator/compare/1.1.0...1.1.1)

> 2023-05-24

### Fix

* AutoChangelog don't load commits

### Merge Requests

* Merge branch 'feature/try_chglog' into 'develop'


<a name="1.1.0"></a>
## [1.1.0](https://gitlab.com/rayformatics-docker-generators/rayls-generator/compare/1.0.0-1...1.1.0)

> 2023-05-24


<a name="1.0.0-1"></a>
## [1.0.0-1](https://gitlab.com/rayformatics-docker-generators/rayls-generator/compare/1.0.0...1.0.0-1)

> 2023-04-25


<a name="1.0.0"></a>
## 1.0.0

> 2023-04-25

